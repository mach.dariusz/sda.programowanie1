package com.sda.kurs.programowanie1.Day4.hashing;

import java.lang.reflect.Array;
import java.util.Arrays;

public class BetterFakeHashStore {

    private static final int STORE_SIZE = 10;
    // added two dimensional array
    private Object[][] store = new Object[STORE_SIZE][4];

    private int storeLocation(Object o) {
        return Math.abs(o.hashCode()) % STORE_SIZE;
    }

    public void insert(Object o) {
        Object[] target = store[storeLocation(o)];
        int index = 0;
        // find target which is not null and increase index
        while (target[index] != null) {
            index++;
        }
        // then insert
        target[index] = o;
    }

    public boolean contains(Object o) {
        Object[] target = store[storeLocation(o)];
        int index = 0;
        // find targets
        while (target[index] != null) {
            // check if are equals and return true if they are
            if (o.equals(target[index])) {
                return true;
            }
            index++; // <- increase index to check all items
        }
        return false; // <- return false if store doesn't contain an item
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Object[] row: store) {
            int index = 0;
            while (row[index] != null) {
                sb.append(row[index]).append(", ");
                index++;
            }
        }
        if (sb.length() >= 2) sb.setLength(sb.length() - 2);

        return sb.toString();
    }


    public static void main(String[] args) {
        BetterFakeHashStore fhs = new BetterFakeHashStore();

        fhs.insert(new Student("Anna"));
        fhs.insert(new Student("Darek"));
        fhs.insert(new Student("Jan"));
        fhs.insert(new Student("Krzysiek"));
        fhs.insert(new Student("Marcin"));
        System.out.println("In the store: " + fhs);


        System.out.println("Contains Anna? " + fhs.contains(new Student("Anna")));
        System.out.println("Contains Darek? " + fhs.contains(new Student("Darek")));

        fhs.insert(new Fruit(Fruit.Color.GREEN));
        fhs.insert(new Fruit(Fruit.Color.RED));
        System.out.println("In the store: " + fhs);

        fhs.insert(new Student("Dariusz Bartosz"));
        fhs.insert(new Fruit(Fruit.Color.BLUE));
        System.out.println("In the store: " + fhs);
        for (int i = 0; i < fhs.store.length; i++) {
            System.out.printf("Array[%d] = %s%n", i, Arrays.toString(fhs.store[i]));
        }

        System.out.println("Contains GREEN? " + fhs.contains(new Fruit(Fruit.Color.GREEN)));
        System.out.println("Contains Daro? " + fhs.contains(new Student("Daro")));

        Student s1 = new Student("Darek");
        Student s2 = new Student("Marek");

        System.out.println(s1.hashCode() == s2.hashCode());
        System.out.println(s1.equals(s2));

        Student s3 = new Student("Darek");
        System.out.println(s1.hashCode() == s2.hashCode());
        System.out.println(s1.equals(s2));


    }

}
