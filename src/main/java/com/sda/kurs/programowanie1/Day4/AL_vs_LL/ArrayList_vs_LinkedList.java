package com.sda.kurs.programowanie1.Day4.AL_vs_LL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ArrayList_vs_LinkedList {


    public static void main(String[] args) {
        final int COUNT = 250_000;

        String[] items = new String[COUNT];
        for (int i = 0; i < COUNT; i++) {
            items[i] = "" + i;
        }

        List<String> starting = Arrays.asList(items);
        List<String> list = new ArrayList<>();
        list.addAll(starting);

        System.out.printf(
                "Time to insert %d items to the ArrayList: " +
                "%,12.6f secs %n",
                COUNT,
                insertInTheMiddle(list, starting) / 1_000_000_000.0);

        list = new LinkedList<>();
        list.addAll(starting);

        System.out.printf(
                "Time to insert %d items to the %s: " +
                        "%,12.6f secs %n",
                COUNT,
                "LinkedList",
                insertInTheMiddle(list, starting) / 1_000_000_000.0);

    }

    private static long insert(List<String> list, List<String> items) {

        int target = 0;
        long start = System.nanoTime();

        for ( String s: items) {
            list.add(target, s);
        }

        long end = System.nanoTime();
        return end - start;
    }

    private static long insertInTheMiddle(List<String> list, List<String> items) {

        int target = list.size() / 2;
        long start = System.nanoTime();

        for ( String s: items) {
            list.add(target, s);
        }

        long end = System.nanoTime();
        return end - start;
    }


}
