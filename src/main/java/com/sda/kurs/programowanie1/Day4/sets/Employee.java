package com.sda.kurs.programowanie1.Day4.sets;

import java.util.Comparator;
import java.util.Objects;

public class Employee {

    private String name;
    private int age;
    public static NameDESC nameDESC = new Employee.NameDESC();

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return age == employee.age &&
                Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    private static class NameDESC implements Comparator<Employee> {
        @Override
        public int compare(Employee e1, Employee e2) {
            return e2.name.compareTo(e1.name);
        }
    }
}

