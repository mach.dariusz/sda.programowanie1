package com.sda.kurs.programowanie1.Day4.hashing;

import java.util.Arrays;

public class FakeHashStore {

    private static final int STORE_SIZE = 10;
    private Object[] store = new Object[STORE_SIZE];

    public void insert(Object o) {
        store[storeLocation(o)] = o;
    }
    public boolean contains(Object o) {
        return store[storeLocation(o)] != null;
    }

    private int storeLocation(Object o) {
        return Math.abs(o.hashCode()) % STORE_SIZE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Object o : store) {
            if (o != null) {
                sb.append(o).append(", ");
            }
        }
        if (sb.length() >= 2) sb.setLength(sb.length() - 2);

        return sb.toString();
    }

    public static void main(String[] args) {
        FakeHashStore fhs = new FakeHashStore();

        fhs.insert(new Student("Anna"));
        fhs.insert(new Student("Darek"));
        fhs.insert(new Student("Jan"));
        fhs.insert(new Student("Krzysiek"));
        fhs.insert(new Student("Marcin"));

        System.out.println("Store contains: " + fhs);
        // System.out.println("Store: " + Arrays.toString(fhs.store));

        System.out.println("Contains Anna? " + fhs.contains(new Student("Anna")));
        System.out.println("Contains Darek? " + fhs.contains(new Student("Darek")));

        fhs.insert(new Fruit(Fruit.Color.GREEN));
        fhs.insert(new Fruit(Fruit.Color.RED));
        System.out.println("In the store: " + fhs);

        fhs.insert(new Student("Dariusz Bartosz"));
        fhs.insert(new Fruit(Fruit.Color.BLUE));
        System.out.println("In the store: " + fhs);
    }
}
