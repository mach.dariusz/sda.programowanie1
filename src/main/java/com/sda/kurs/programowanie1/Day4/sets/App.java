package com.sda.kurs.programowanie1.Day4.sets;

import java.util.*;

public class App {

    public static void main(String[] args) {

        // hashSetMethod();
        // sortingCollections();
        treeSetMethod();

    }

    private static void hashSetMethod() {
        Set<Employee> set = new HashSet<>();

        set.add(new Employee("Darek", 32));
        set.add(new Employee("Darek", 32));
        set.add(new Employee("Darek", 32));
        set.add(new Employee("Bartek", 32));
        set.add(new Employee("Bartek", 32));
        set.add(new Employee("Bartek", 32));
        set.add(new Employee("Anna", 32));
        set.add(new Employee("Anna", 32));
        set.add(new Employee("Anna", 32));
        set.add(new Employee("Krzysiek", 32));
        set.add(new Employee("Krzysiek", 32));
        set.add(new Employee("Krzysiek", 32));
        set.add(new Employee("Krzysiek", 32));
        set.add(new Employee("Marcin", 32));
        set.add(new Employee("Marcin", 32));
        set.add(new Employee("Marcin", 32));

        System.out.println("Size: " + set.size());
        System.out.println("Set contains: " + set);
    }

    private static void sortingCollections() {
        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Darek", 32));
        list.add(new Employee("Bartek", 32));
        list.add(new Employee("Anna", 32));
        list.add(new Employee("Krzysiek", 32));
        list.add(new Employee("Marcin", 32));

        Iterator<Employee> iterator = list.listIterator();
        while (iterator.hasNext()) {
            System.out.printf("%s%n", iterator.next());
        }
        System.out.println(" ----------------- ");

        // list.sort(Employee.nameDESC);
        list.sort((s1, s2) -> s2.getName().compareTo(s1.getName()));
        System.out.println("After sorting: ");
        for (Employee e : list) {
            System.out.println(e);
        }
    }

    private static void treeSetMethod() {
        // Set<Employee> set = new TreeSet<>((s1, s2) -> s2.getName().compareTo(s1.getName())); // <- per name desc
        // Set<Employee> set = new TreeSet<>(Employee.nameDESC); // <- per name desc
        /* -> per last char <-
        Set<Employee> set = new TreeSet<>((s1, s2) ->
                s2.getName().charAt(s2.getName().length() - 1)
                        - s1.getName().charAt(s1.getName().length() - 1));
        */

        // Set<Employee> set = new TreeSet<>((s1, s2) -> s1.getName().length() - s2.getName().length());
        Set<Employee> set = new TreeSet<>((s1, s2) -> s1.getAge() - s2.getAge());

        set.add(new Employee("Magda", 20));
        set.add(new Employee("Darek", 32));
        set.add(new Employee("Darek", 32));
        set.add(new Employee("Darek", 32));
        set.add(new Employee("Bartek", 22));
        set.add(new Employee("Bartek", 22));
        set.add(new Employee("Bartek", 22));
        set.add(new Employee("Anna", 18));
        set.add(new Employee("Anna", 18));
        set.add(new Employee("Anna", 18));
        set.add(new Employee("Krzysiek", 31));
        set.add(new Employee("Krzysiek", 31));
        set.add(new Employee("Krzysiek", 31));
        set.add(new Employee("Krzysiek", 31));
        set.add(new Employee("Marcin", 33));
        set.add(new Employee("Marcin", 33));
        set.add(new Employee("Marcin", 34));


        for (Employee e : set) {
            System.out.printf("%s%n", e);
        }
    }
}
