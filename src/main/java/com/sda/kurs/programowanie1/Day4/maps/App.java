package com.sda.kurs.programowanie1.Day4.maps;

import com.sda.kurs.programowanie1.Day4.sets.Employee;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class App {

    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "HR");
        map.put(2, "Payroll");
        map.put(3, "IT");

        // System.out.println(map);

        /*  -> uncomment to test <-

        iterationOverKeys(map);

        iterationOverValues(map);

        iterationOverEntries(map);
        */

        map.forEach((K, V) -> System.out.printf("key: %d; value: %s%n", K, V));
        // below lines are doing the same
        Stream.of(map).filter(m -> m.containsValue("IT")).forEach(System.out::println);
        Stream.of(map).filter(m -> m.containsValue("IT")).forEach(m -> System.out.println(m));

        employeeExample();

    }

    private static void iterationOverEntries(Map<Integer, String> map) {
        System.out.println("Iteration over entries (key, value):");
        for (Map.Entry<Integer, String> entry: map.entrySet()) {
            System.out.printf("key: %d; value: %s%n", entry.getKey(), entry.getValue());
        }
    }

    private static void iterationOverValues(Map<Integer, String> map) {
        System.out.println("Iteration over values:");
        for (String value: map.values()) {
            System.out.println("value: " + value);
        }
    }

    private static void iterationOverKeys(Map<Integer, String> map) {
        System.out.println("Iteration over keys:");
        for (Integer key: map.keySet()) {
            System.out.println("key: " + key);
        }
    }

    private static void employeeExample() {
        Map<Integer, Employee> map = new HashMap<>();

        map.put(1, new Employee("Darek", 32));

        if (map.containsKey(1)) {
            System.out.printf("Map contains key: %d; its value is: %s", 1, map.get(1));
        }
    }

}
