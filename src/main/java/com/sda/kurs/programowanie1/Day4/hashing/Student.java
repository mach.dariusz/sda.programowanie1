package com.sda.kurs.programowanie1.Day4.hashing;

public final class Student {
    private final String name;

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.length();
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass() != Student.class) return false;
        Student otherStudent = (Student) other;
        return otherStudent.name.equals(name);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
