package com.sda.kurs.programowanie1.Day2;

public class App {
    private final static MyQueue<String> queue = new MyQueue<>(5);

    public static void main(String[] args) {
        queue.add("A");
        queue.add("B");
        queue.add("C");
        queue.add("D");
        queue.add("E");

        queue.printState();

    }
}
