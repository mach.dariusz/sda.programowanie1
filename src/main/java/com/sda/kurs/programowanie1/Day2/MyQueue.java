package com.sda.kurs.programowanie1.Day2;

public class MyQueue<E> {

    private E[] queue;

    //The current number of items in the queueThe maximum number of items that can be held (the length of the array)
    private int capacity;

    // The current number of items in the queue
    private int size;

    // The index into the array where the next item will be added
    private int head;

    // The index into the array where the next item will be removed
    private int tail;

    public MyQueue(int capacity) {
        this.capacity = capacity > 0 ? capacity : 1; // at least 1
        this.queue = (E[]) new Object[capacity];
        this.head = 0;
        this.tail = 0;
        this.size = 0;
    }

    public boolean add(E e) {
        if (!isFull()) {
            this.queue[this.head] = e;
            this.head = (this.head + 1) % this.capacity;
            this.size++;
            return true;

        } else {
            throw new IllegalStateException("CustomFIFO is full!");
        }
    }

    public E remove() {
        if (!isEmpty()) {
            E e = this.queue[this.tail];
            this.queue[this.tail] = null; // don't block GC by keeping reference
            this.tail = (this.tail + 1) % this.capacity;
            this.size--;

            return e;
        } else {
            return null;
        }
    }

    public E peek() {
        return this.queue[this.tail];
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean isFull() {
        return this.size == this.capacity;
    }

    public int getSize() {
        return this.size;
    }


    public void printState() {
        StringBuffer sb = new StringBuffer();
        sb.append("CustomFIFO:\n");
        sb.append("       capacity=" + this.capacity + "\n");
        sb.append("           size=" + this.size);
        if (isFull()) {
            sb.append(" - FULL");
        } else if (size == 0) {
            sb.append(" - EMPTY");
        }
        sb.append("\n");
        sb.append("           head=" + this.head + "\n");
        sb.append("           tail=" + this.tail + "\n");
        for (int i = 0; i < this.queue.length; i++) {
            sb.append("       queue[" + i + "]=" + this.queue[i] + "\n");
        }
        System.out.print(sb);
    }

}
