package com.sda.kurs.programowanie1.Day2;

public class MyStack<E> {

    private E[] stack;

    //The current number of items in the queueThe maximum number of items that can be held (the length of the array)
    private int capacity;

    // The current number of items in the queue
    private int size;

    // The index into the array where the next item will be added
    private int head;

    public MyStack(int capacity) {
        this.capacity = capacity > 0 ? capacity : 1; // at least 1
        this.stack = (E[]) new Object[capacity];
        this.head = 0;
        this.size = 0;
    }

    public boolean push(E e) {
        if (!isFull()) {
            this.stack[this.head] = e;
            this.head = this.head == this.capacity ? 0 : this.head + 1;
            this.head = (this.head + 1) % this.capacity;
            this.size++;

            return true;
        } else {
            return false;
        }
    }

    public E pop() {
        if (!isEmpty()) {
            E e = this.stack[this.head - 1];
            this.stack[this.head - 1] = null;
            this.head--;
            this.size--;

            return e;
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public boolean isFull() {
        return this.size == this.capacity;
    }

    public int getSize() {
        return this.size;
    }

    public void printState() {
        StringBuffer sb = new StringBuffer();
        sb.append("CUSTOMFIFO:\n");
        sb.append("       capacity=" + this.capacity + "\n");
        sb.append("           size=" + this.size);
        if (isFull()) {
            sb.append(" - FULL");
        } else if (size == 0) {
            sb.append(" - EMPTY");
        }
        sb.append("\n");
        sb.append("           head=" + this.head + "\n");
        for (int i = 0; i < this.stack.length; i++) {
            sb.append("       queue[" + i + "]=" + this.stack[i] + "\n");
        }
        System.out.print(sb);
    }

}
