package com.sda.kurs.programowanie1.Day3.streams;

public enum EmployeePosition {

    MANAGER,
    REGULAR,
    BOSS
}
