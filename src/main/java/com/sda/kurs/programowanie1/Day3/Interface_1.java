package com.sda.kurs.programowanie1.Day3;

@FunctionalInterface
public interface Interface_1 {

    double calculate(double x);

    default void print() {
        System.out.println("Hello from Interface_1");
    }

    static void printStatic() {
        System.out.println("Hello from static method");
    };
}
