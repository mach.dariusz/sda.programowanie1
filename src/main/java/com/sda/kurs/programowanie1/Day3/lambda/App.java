package com.sda.kurs.programowanie1.Day3.lambda;

import java.util.Arrays;
import java.util.Comparator;

public class App {

    private static String[] ARRAY = {"A", "Z", "B", "X", "C", "U", "D", "E"};
    private static String[] ARRAY2 = {"A", "Z", "B", "X", "C", "U", "D", "E"};

    public static void main(String[] args) {
        System.out.println("START: " + Arrays.toString(ARRAY));
//        sortOldWay_asc();
//        System.out.println("AFTER OLD WAY SORTING - ASC: " + Arrays.toString(ARRAY));
//        sortOldWay_2_asc();
//        System.out.println("AFTER OLD WAY SORTING - ASC: " + Arrays.toString(ARRAY));
//        sortOldWay_desc();
//        System.out.println("AFTER OLD WAY SORTING - DESC: " + Arrays.toString(ARRAY));
//        sortOldWay_2_desc();
//        System.out.println("AFTER OLD WAY SORTING - DESC: " + Arrays.toString(ARRAY));
//
//        System.out.println();
//        System.out.println("START: " + Arrays.toString(ARRAY2));
//        Arrays.sort(ARRAY2, new SortString_DESC());
//        System.out.println("AFTER OLD WAY SORTING - DESC: " + Arrays.toString(ARRAY2));


//        Arrays.sort(ARRAY, (s1, s2) -> s1.charAt(s1.length() - 1) - s2.charAt(s2.length() -1 ));

        Arrays.sort(ARRAY, Comparator.comparingInt(s -> s.charAt(s.length() - 1)));
        System.out.println(Arrays.toString(ARRAY));
        Arrays.sort(ARRAY, (s1, s2) -> s2.charAt(s2.length() -1) - s1.charAt(s1.length() - 1));
        System.out.println(Arrays.toString(ARRAY));

        // I would like to debug this:
        sortOldWay_desc();
    }

    private static void sortOldWay_asc() {
        Arrays.sort(ARRAY, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.charAt(s1.length() -1) - s2.charAt(s2.length() -1);
            }
        });
    }

    private static void sortOldWay_2_asc() {
        Arrays.sort(ARRAY, new SortString_ASC());
    }

    private static void sortOldWay_desc() {
        Arrays.sort(ARRAY, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.charAt(s2.length() -1) - s1.charAt(s1.length() -1);
            }
        });
    }

    private static void sortOldWay_2_desc() {
        Arrays.sort(ARRAY, new SortString_DESC());
    }
}
