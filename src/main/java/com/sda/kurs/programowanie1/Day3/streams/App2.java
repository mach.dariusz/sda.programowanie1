package com.sda.kurs.programowanie1.Day3.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class App2 {

    private static final List<Employee> LISTA = new ArrayList<>();

    public static void main(String[] args) {
        Employee emp1 = generateEmployee();
        Employee emp2;

        LISTA.add(emp1);
        emp2 = LISTA.get(LISTA.size() - 1);
        LISTA.add(emp2);

        LISTA.stream().forEach(emp -> System.out.println(emp));
        emp1.setFirstName("Anna");
        LISTA.stream().forEach(emp -> System.out.println(emp));

        printListOfEmployees(LISTA, true);
        emp1.setFirstName("Anna");
        printListOfEmployees(LISTA, false);

    }

    private static Employee generateEmployee() {


        Address address = new Address();
        address.setCountry("Polska");
        address.setCity("Rzeszów");
        address.setStreet("Langiewicza");
        address.setBuildNumber("2A");
        address.setFlatNumber("4");
        address.setPostalCode("35-245");

        Company company = new Company();
        company.setAddress(address);
        company.setName("HETMAN");
        company.setPosition(EmployeePosition.BOSS);
        company.setSalary(10_000.00);

        Employee emp = new Employee();
        emp.setFirstName("John");
        emp.setLastName("Wayne");
        emp.setAddress(address);
        emp.setCompany(company);

        return emp;
    }

    private static void printListOfEmployees(List<Employee> lista, boolean nadpisz) {
        Employee e = lista.get(0);
        if (nadpisz) {
            e.setFirstName("Darek");
        }
        lista.stream().forEach(emp -> System.out.println(emp));
    }

}
