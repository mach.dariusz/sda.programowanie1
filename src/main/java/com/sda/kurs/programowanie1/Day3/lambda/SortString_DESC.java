package com.sda.kurs.programowanie1.Day3.lambda;

import java.util.Comparator;

public class SortString_DESC implements Comparator<String> {

    @Override
    public int compare(String s1, String s2) {
        return s2.charAt(s2.length() -1) - s1.charAt(s1.length() -1);
    }
}
