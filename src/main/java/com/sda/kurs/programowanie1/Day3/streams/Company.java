package com.sda.kurs.programowanie1.Day3.streams;

public class Company {

    private String name;
    private Enum<EmployeePosition> position;
    private Address address;
    private double salary;

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", position=" + position +
                ", address=" + address +
                ", salary=" + salary +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enum<EmployeePosition> getPosition() {
        return position;
    }

    public void setPosition(Enum<EmployeePosition> position) {
        this.position = position;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
