package com.sda.kurs.programowanie1.Day3.streams;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class App {

    static AtomicInteger counter = new AtomicInteger();


    private static String[] ARRAY = {"A", "A", "A", "Z", "B", "X", "B", "X", "C", "U", "D", "E"};
    private static Integer[] ARRAY_OF_INTS = {2, 4, 8, 10, 12, 14, 16};

    public static void main(String[] args) {
        filterElements();

        forEachAndPrint();

        filterAndDistinct();

        filterAndFindFirst();

        checkingOptional();

        convertingIntsArrayToStringsArray();

        findFirstAndOrElse();

        flatMapOnArraysOfRandomLetters();

        counterInForEach();
    }

    private static void counterInForEach() {

        Stream.of(ARRAY_OF_INTS).filter(e -> e >= 10).forEach(element -> {
            System.out.printf("counter: %d - el: %s %n", counter.getAndIncrement(), element);
        });
    }

    private static void flatMapOnArraysOfRandomLetters() {
        String[][] arrayOfRandomLetters = {
                {"a", "b", "c"}, // c - arrayOfRandomLetters[0][2]
                {"d", "e", "f"}, // e - arrayOfRandomLetters[1][1]
                {"g", "h", "i"}, // g - arrayOfRandomLetters[2][0]
        };

        String[] arrayOfRandomLetters2 = Stream.of(arrayOfRandomLetters)
                .flatMap(elements -> Stream.of(elements)).toArray(String[]::new);
        System.out.println(Arrays.toString(arrayOfRandomLetters2));
    }

    private static void findFirstAndOrElse() {
        System.out.println(Stream.of(ARRAY_OF_INTS).filter(e -> e > 10).findFirst().orElse(null));
    }

    private static void convertingIntsArrayToStringsArray() {
        String[] arrayOfStrings = Stream.of(ARRAY_OF_INTS).map(liczba -> "Liczba przekazana to: " + liczba).peek(e -> {
            e = "AAA";
            System.out.println(e);
        }).toArray(String[]::new);
        System.out.println(Arrays.toString(arrayOfStrings));
    }

    private static void checkingOptional() {
        Optional<String> optional = Optional.empty();
        System.out.println("optional.isPresent(): " + optional.orElse("Nic tam nie ma!"));
    }

    private static void filterAndFindFirst() {
        System.out.println("op: " +
                Stream.of(ARRAY_OF_INTS).filter(x -> x == 9).findFirst().orElse(-10));
    }

    private static void filterAndDistinct() {
        Arrays.stream(ARRAY).filter(e -> !e.equals("X")).distinct().forEach(System.out::print);
        System.out.println();
        String[] temp = Arrays.stream(ARRAY).filter(e -> !e.equals("X")).distinct().toArray(String[]::new);
        System.out.println(Arrays.toString(temp));
    }

    private static void forEachAndPrint() {
        Arrays.stream(ARRAY).distinct().forEach(System.out::print);
        System.out.println();
    }

    private static void filterElements() {
        Arrays.stream(ARRAY).filter(element -> !element.equals("A")).forEach(System.out::print);
        System.out.println();
        Arrays.stream(ARRAY).filter(element -> !element.equals("B")).forEach(System.out::print);
        System.out.println();
    }
}
