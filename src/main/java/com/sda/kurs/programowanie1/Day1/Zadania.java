package com.sda.kurs.programowanie1.Day1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Zadania {

    private static final Scanner SCANNER = new Scanner(System.in);

    /*
     * Napisz metodę, która sprawdza pełnoletność danej osoby.
     * Zastanów się nad sygnaturą metody, następnie ją zaimplementuj i uruchom dla kilku wartości
     * */
    public static void zadanie_1() {

        System.out.println("Podaj swój wiek:");
        int age = SCANNER.nextInt();

        /*
        * Rownowazne z tym co u dolu.
        if (age >= 18) {
            System.out.println("Jesteś pełnoletni(a)");
        } else {
            System.out.println("Nie jestes pełnoletni(a)");
        }
        */

        System.out.println(age >= 18 ? "Jesteś pełnoletni(a)" : "Nie jestes pełnoletni(a)");
    }

    /*
     * Napisz program, który rysuje choinkę ze znaczków '*',
     * tj. trójkąt równoramienny a pod nim prostokąt.
     */
    public static void zadanie_2() throws Exception {
        DrawUtils.drawChristmasTree(10, '*');

        for (int i = 0; i < 10; i++) {
            DrawUtils.drawLine(10 * 2, '*');
            System.out.println();
        }
    }

    /*
     * Dana jest tablica liczb całkowitych, wypisz:
     * */

    // a) wszystkie po kolei
    public static int[] zadanie_3_a(int[] array) {

        return array;
    }

    // b) wszystkie od końca
    public static int[] zadanie_3_b(int[] array) {
        int counter = 0;
        int[] reverseArray = new int[array.length];

        for (int i = array.length; i > 0; i--) {
//            System.out.println(i + ": " + array[i - 1]);
            reverseArray[counter++] = array[i - 1];
        }
        return reverseArray;
    }

    // c) wszystkie na nieparzystych pozycjach
    public static int[] zadanie_3_c(int[] array) {
        int[] tempArray = new int[0];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0) {
                tempArray = Arrays.copyOf(tempArray, tempArray.length + 1);
                tempArray[j++] = array[i];
            }

        }
        return tempArray;
    }

    // d) wszystkie podzielne przez 3
    public static int[] zadanie_3_d(int[] array) {
        int[] tempArray = new int[0];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 3 == 0) {
                tempArray = Arrays.copyOf(tempArray, tempArray.length + 1);
                tempArray[j++] = array[i];
            }

        }
        return tempArray;
    }

    // e) sumę wszystkich
    public static int zadanie_3_e(int[] array) {
        int suma = 0;

        for (int item: array) {
            suma += item;
        }

        return suma;
    }

    // f) sumę pierwszych 4
    public static int zadanie_3_f(int[] array) {
        int suma = 0;

        for (int i = 0; i < 4; i++) {
            suma += array[i];
        }

        return suma;
    }

    // g) sumę ostatnich 5 większych od 2
    public static int zadanie_3_g(int[] array) {
        int[] reverseArray = zadanie_3b(array);

        int suma = 0;

        for (int i = 0; i < 5; i++) {
            if (reverseArray[i] > 2) suma += reverseArray[i];
        }

        return suma;
    }

    // h) ilość liczb idąc od początku tablicy, by ich suma przekroczyła 200
    public static int zadanie_3_h(int[] array) {
        int suma = 0;
        for (int i = 0; i < array.length; i++) {
            suma += array[i];
            if (suma > 200)
                return i + 1;
        }

        return array.length;
    }


    /*
     * Alternatywne rozwiazania:
     * */

    public static int[] zadanie_3b(int[] array) {
        List<Integer> list = Arrays.stream(array).boxed().collect(Collectors.toList());
        Collections.reverse(list);
        return list.stream().mapToInt(value -> value).toArray();
    }

    public static int[] zadanie_3d(int[] array) {
        return Arrays.stream(array).filter(item -> item % 3 == 0).toArray();
    }

    public static int zadanie_3e(int[] array) {
        return Arrays.stream(array).reduce(0, (i1, i2) -> i1 + i2);
    }

    public static int zadanie_3f(int[] array) {
        return Arrays.stream(array).limit(4).reduce(0, (i1, i2) -> i1 + i2);
    }

    public static int zadanie_3g(int[] array) {
        List<Integer> list = Arrays.stream(array).boxed().collect(Collectors.toList());
        Collections.reverse(list);
        return list.stream().limit(5).filter(item -> item > 2).reduce(0, (i1, i2) -> i1 + i2);
    }

}
