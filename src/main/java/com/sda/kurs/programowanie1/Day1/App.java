package com.sda.kurs.programowanie1.Day1;

import java.util.Arrays;
import java.util.Random;

public class App {

    public static void main(String[] args) throws Exception {

        System.out.println("Zadanie_1");
        Zadania.zadanie_1();
        System.out.println("-------------------------------------------");
        System.out.println();

        System.out.println("Zadanie_2");
        Zadania.zadanie_2();
        System.out.println("-------------------------------------------");
        System.out.println();

        int[] array = generateArrayOfRandomNumbers(10, 100);
        System.out.println("Zadanie_3\nMain array: " + Arrays.toString(array));
        System.out.println();

        System.out.println("Zadanie_3a - wszystkie po kolei: " + Arrays.toString(Zadania.zadanie_3_a(array)));
        System.out.println("Zadanie_3b - wszystkie od końca: " + Arrays.toString(Zadania.zadanie_3_b(array)));
        System.out.println("Zadanie_3b -       alternatywne: " + Arrays.toString(Zadania.zadanie_3b(array)));
        System.out.println("Zadanie_3c - wszystkie na nieparzystych pozycjach: " + Arrays.toString(Zadania.zadanie_3_c(array)));
        System.out.println("Zadanie_3d - wszystkie podzielne przez 3: " + Arrays.toString(Zadania.zadanie_3_d(array)));
        System.out.println("Zadanie_3d -                alternatywne: " + Arrays.toString(Zadania.zadanie_3d(array)));
        System.out.println("Zadanie_3e - sumę wszystkich: " + Zadania.zadanie_3_e(array));
        System.out.println("Zadanie_3e -    alternatywne: " + Zadania.zadanie_3_e(array));
        System.out.println("Zadanie_3f - sumę pierwszych 4: " + Zadania.zadanie_3_f(array));
        System.out.println("Zadanie_3f -      alternatywne: " + Zadania.zadanie_3_f(array));
        System.out.println("Zadanie_3g - sumę ostatnich 5 większych od 2: " + Zadania.zadanie_3_g(array));
        System.out.println("Zadanie_3g -                    alternatywne: " + Zadania.zadanie_3_g(array));
        System.out.println("Zadanie_3h - ilość liczb idąc od początku tablicy, by ich suma przekroczyła 200: " + Zadania.zadanie_3_h(array));


    }

    private static int[] generateArrayOfRandomNumbers(int numberOfElements, int range) {
        int[] array = new int[numberOfElements];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(range);
        }

        return array;
    }
}
