package com.sda.kurs.programowanie1.Day1;

public class DrawUtils {

    private static final char EMPTY_CHAR = ' ';

    public static void main(String[] args) throws Exception {
        /*
         * examples of execution
         * */
        drawChristmasTree(3, '*');
        drawChristmasTree(4, '*');
        drawChristmasTree(5, '*');
        drawChristmasTree(6, '*');
        drawChristmasTree(7, '*');
        drawChristmasTree(8, '*');
        drawChristmasTree(9, '*');
        drawChristmasTree(10, '*');
        drawChristmasTree(10, '!');
        drawChristmasTree(10, '@');
        drawChristmasTree(10, '#');
        drawChristmasTree(10, '$');
        drawChristmasTree(10, '%');
        drawChristmasTree(10, '^');
        drawChristmasTree(10, '&');
        drawChristmasTree(10, ':');
        drawChristmasTree(10, '|');
    }

    public static void drawChristmasTree(int treeHeight, char character) throws Exception {
        if (treeHeight < 3) {
            throw new Exception("Our christmas tree can't be lower than 3 rows of branches!");
        } else {
            System.out.println("Christmas tree - height: " + treeHeight);
            drawTree(treeHeight, character);
            drawTrunk(treeHeight, character);
        }

    }

    public static void drawTree(int treeHeight, char character) {
        for (int i = 0; i < treeHeight; i++) {
            drawLine(treeHeight - (i + 1), EMPTY_CHAR);
            drawLine(i * 2 + 1, character);
            System.out.println();
        }
    }

    public static void drawTrunk(int treeHeight, char character) {
        int numberOfCharsInLine = treeHeight * 2 - 1;
        int trunkWidth = treeHeight > 5 ? 3 : 1;

        for (int i = 0; i < 2; i++) {
            int howManyEmptyChars = treeHeight > 5 ? numberOfCharsInLine / 2 - 1 : numberOfCharsInLine / 2;
            drawLine(howManyEmptyChars, EMPTY_CHAR);
            drawLine(trunkWidth, character);
            System.out.println();
        }
        System.out.println();
    }

    public static void drawLine(int width, char character) {
        for (int i = 0; i < width; i++) {
            System.out.print(character);
        }
    }
}
