package com.sda.kurs.programowanie1.exceptions;

public class OutOfRangeNumberException extends RuntimeException {

    public OutOfRangeNumberException(String errorMessage) {
        super(errorMessage);
    }
}
