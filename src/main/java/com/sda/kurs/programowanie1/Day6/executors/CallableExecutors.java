package com.sda.kurs.programowanie1.Day6.executors;

import java.util.concurrent.*;

public class CallableExecutors {

    public static void main(String[] args) {

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(2);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<Integer> future = executor.submit(task);

        try {
            System.out.println("future done? " + future.isDone());

            Integer result = future.get();

            System.out.println("future done? " + future.isDone());
            System.out.println("result: " + result);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            // remember about shutdown
            executor.shutdown();
            System.out.println("Shutdown finished");
        }

    }

}
