package com.sda.kurs.programowanie1.Day6.begin;

public class ThreadExample implements Runnable {

    @Override
    public void run() {
        System.out.println("Hello from " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        ThreadExample te = new ThreadExample();

        // System.out.println(Thread.currentThread().getName());

        Thread t1 = new Thread(te);
        Thread t2 = new Thread(te);
        Thread t3 = new Thread(te);
        Thread t4 = new Thread(te);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
