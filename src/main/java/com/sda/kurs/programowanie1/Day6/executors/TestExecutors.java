package com.sda.kurs.programowanie1.Day6.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestExecutors {

    public static void main(String[] args) {
        // singleExecutor();
        singleExecutorWithShutdown();
    }


    /**
     * This java process never stops!
     * SynchronizationExamples have to be stopped explicitly - otherwise they keep listening for new tasks.
     */
    private static void singleExecutor() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
        });
    }

    private static void singleExecutorWithShutdown() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        try {
            System.out.println("attempt to shutdown executor");
            executor.awaitTermination(5, TimeUnit.SECONDS);
            executor.shutdown();

        }
        catch (InterruptedException e) {
            System.err.println("tasks interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                System.err.println("cancel non-finished tasks");
            }
            executor.shutdownNow();
            System.out.println("shutdown finished");
        }
    }
}
