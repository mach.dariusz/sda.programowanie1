package com.sda.kurs.programowanie1.Day6.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static com.sda.multithreading.sync.ConcurrentUtils.stop;

public class SyncExample {
    int count = 0;

    void increment() {
        count = count + 1;
    }

    int syncCount = 0;

    // synchronized void syncIncrement() {
    //     syncCount = syncCount + 1;
    // }

    void syncIncrement() {
        synchronized (this) {
            syncCount = syncCount + 1;
        }
    }


    public static void main(String[] args) {

        SyncExample se = new SyncExample();

        se.incrementAndPrint();

        se.syncIncrementAndPrint();

    }

    private void incrementAndPrint() {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, 10000)
                .forEach(i -> executor.submit(this::increment));


        stop(executor);

        System.out.println(this.count);

    }

    private void syncIncrementAndPrint() {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, 10000)
                .forEach(i -> executor.submit(this::syncIncrement));

        stop(executor);

        System.out.println(this.syncCount);  // 10000

    }
}
