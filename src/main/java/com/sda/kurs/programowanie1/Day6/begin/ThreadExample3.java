package com.sda.kurs.programowanie1.Day6.begin;

public class ThreadExample3 {


    static Runnable task = () -> {
        System.out.println("ver 3: Hello from " + Thread.currentThread().getName());
    };

    public static void main(String[] args) {

        Thread t1 = new Thread(task);
        Thread t2 = new Thread(task);
        Thread t3 = new Thread(task);
        Thread t4 = new Thread(task);


        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
