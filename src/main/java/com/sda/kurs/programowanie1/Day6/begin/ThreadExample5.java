package com.sda.kurs.programowanie1.Day6.begin;

import java.util.concurrent.TimeUnit;

public class ThreadExample5 {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            try {
                String name = Thread.currentThread().getName();
                System.out.println("Foo " + name);

                Thread.sleep(2000);
                System.out.println("Bar " + name);

                TimeUnit.SECONDS.sleep(2);
                System.out.println("Ear " + name);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);
        Thread t3 = new Thread(runnable);
        Thread t4 = new Thread(runnable);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
