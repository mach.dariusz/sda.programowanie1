package com.sda.kurs.programowanie1.Day6.begin;

import java.util.concurrent.TimeUnit;

public class ThreadExample2 extends Thread {

    @Override
    public void run() {
        System.out.println("ver 2: Hello from " + Thread.currentThread().getName());
    }

    public static void main(String[] args) {

        Thread t1 = new ThreadExample2();
        Thread t2 = new ThreadExample2();
        Thread t3 = new ThreadExample2();
        Thread t4 = new ThreadExample2();

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
