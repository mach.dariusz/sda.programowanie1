package com.sda.kurs.programowanie1.Day6.executors;

import java.util.concurrent.*;

public class CallableExecutors2 {

    public static void main(String[] args) {

        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(6);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };

        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<Integer> future = executor.submit(task);

        try {
            System.out.println("future done? " + future.isDone());

            Integer result = future.get(5, TimeUnit.SECONDS);

            System.out.println("future done? " + future.isDone());
            System.out.println("result: " + result);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } finally {
            // remember about shutdown
            executor.shutdownNow();
            System.out.println("Shutdown finished");
        }

    }

}
