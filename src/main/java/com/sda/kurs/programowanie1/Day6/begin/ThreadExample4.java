package com.sda.kurs.programowanie1.Day6.begin;

public class ThreadExample4 {

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> System.out.println("ver 4: Hello from " + Thread.currentThread().getName()));
        Thread t2 = new Thread(() -> System.out.println("ver 4: Hello from " + Thread.currentThread().getName()));
        Thread t3 = new Thread(() -> System.out.println("ver 4: Hello from " + Thread.currentThread().getName()));

        t1.start();
        t2.start();
        t3.start();
    }
}
