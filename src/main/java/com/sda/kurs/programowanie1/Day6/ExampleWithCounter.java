package com.sda.kurs.programowanie1.Day6;

import com.sda.kurs.programowanie1.Day5.eatingExcercise.Employee;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ExampleWithCounter {

    static AtomicInteger counter = new AtomicInteger();

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + " = " +list.get(i));
        }

        list.stream().forEach(item -> System.out.println(counter.getAndIncrement() + " = " + item));


    }
}
