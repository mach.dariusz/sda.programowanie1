package com.sda.kurs.programowanie1.Day6.stack;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class App {

    private final static MyStack<String> STACK = new MyStack<>(5);

    public static void main(String[] args) {

        try {
            STACK.push(getRandomItem());
            STACK.push(getRandomItem());
            STACK.push(getRandomItem());
            STACK.push(getRandomItem());
            STACK.push(getRandomItem());
            STACK.printState();

            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread consumer = new Thread(() -> {
            System.out.println("I am hungry ...");
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(1);

                    System.out.println("Consumed: " + STACK.pop());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread producer = new Thread(() -> {
            System.out.println("I am going for a shoping ...");
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(3);
                    String item = getRandomItem();
                    STACK.push(item);

                    System.out.println("Added: " + item);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        consumer.start();
        producer.start();
    }



    private static String getRandomItem() {

        String[] items = {"Milk", "Coffee", "Sugar", "Chocolate", "Ham"};
        Random random = new Random();

        int randomNumber = random.nextInt(10);

        return items[random.nextInt(5)];
    }

}
