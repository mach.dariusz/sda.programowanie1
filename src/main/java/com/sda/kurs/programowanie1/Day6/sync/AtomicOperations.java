package com.sda.kurs.programowanie1.Day6.sync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static com.sda.multithreading.sync.ConcurrentUtils.stop;

public class AtomicOperations {

    public static void main(String[] args) {
        AtomicOperations ao = new AtomicOperations();

        ao.increment();
    }

    private void increment() {
        AtomicInteger atomicInt = new AtomicInteger(0);

        ExecutorService executor = Executors.newFixedThreadPool(2);

        IntStream.range(0, 10000)
                .forEach(i -> executor.submit(atomicInt::incrementAndGet));

        stop(executor);

        System.out.println(atomicInt.get());    // => 1000
    }

}
