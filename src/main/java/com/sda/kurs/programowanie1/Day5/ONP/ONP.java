package com.sda.kurs.programowanie1.Day5.ONP;

import java.util.Stack;

public class ONP {

    private static final Stack<Double> STACK = new Stack<>();

    /*
     * Krok 1:	Czytaj element z wyrazenia ONP
     * Krok 2:	Jeśli element nie jest liczbą, to idź do kroku 5
     * Krok 3:	Umieść element na stosie
     * Krok 4:	Idź do kroku 1
     * Krok 5:	Jeśli element jest znakiem '=', to idź do kroku 10
     * Krok 6:	Pobierz ze stosu dwie liczby a i b
     * Krok 7:	Wykonaj nad liczbami a i b operację określoną przez element i umieść wynik w zmiennej result
     * Krok 8:	Umieść result na stosie
     * Krok 9:	Idź do kroku 1
     * Krok 10:	Prześlij na wyjście zawartość wierzchołka stosu
     * Krok 11:	Zakończ
     */

    public static double calculateONP(String ONP_expression) {
        double result = 0;
        char[] elements = ONP_expression.toCharArray();


        for (int i = 0; i < elements.length; i++) {

            // check if char is not a digit
            if (!Character.isDigit(elements[i])) {

                // check if is equals sign
                if (isEqualSign(elements[i])) {
                    return STACK.pop();
                }

                // calculate result and push to STACK
                result = calculateTwoNumber(STACK.pop(), STACK.pop(), elements[i]);
                putElementInStack(result);
            } else {

                // it is a digit
                putElementInStack(Character.digit(elements[i],10));
            }
        }

        return 0;
    }

    private static void putElementInStack(double number) {
        STACK.push(number);
    }

    private static double calculateTwoNumber(Double firstNumber, Double secondNumber, char operator) {

        switch (operator) {
            case '+':
                return secondNumber + firstNumber;
            case '-':
                return secondNumber - firstNumber;
            case '*':
                return secondNumber * firstNumber;
            case '/':
                return secondNumber / firstNumber;
            case '^':
                return Math.pow(secondNumber, firstNumber);
        }
        return 0;
    }

    private static boolean isEqualSign(char element) {
        if (element == '=') {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

        String operation_1 = "22+=";
        String operation_2 = "52-=";
        String operation_3 = "22*=";
        String operation_4 = "62/=";
        String operation_5 = "32^=";
        // (7 + 3) * (5 - 2) ^ 2 =
        String operation_6 = "73+52-2^*=";

        System.out.println(ONP.calculateONP(operation_1));
        System.out.println(ONP.calculateONP(operation_2));
        System.out.println(ONP.calculateONP(operation_3));
        System.out.println(ONP.calculateONP(operation_4));
        System.out.println(ONP.calculateONP(operation_5));
        System.out.println(ONP.calculateONP(operation_6));
    }
}
