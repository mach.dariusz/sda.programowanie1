package com.sda.kurs.programowanie1.Day5;

/**
 * Lista kroków:
 * K01:	Dopóki a ≠ b wykonuj krok K02
 * K02:	    Jeśli a < b, to b ← b - a
 * inaczej          a ← a - b	;
 * od większej liczby odejmujemy mniejszą aż się zrównają
 * K03:	Pisz a	; wtedy dowolna z nich jest NWD
 * K04:	Zakończ
 */
public class NWD {

    public static int calculateNWD(int number1, int number2) {
        while (number1 != number2) {
            if (number1 < number2) {
                number2 = number2 - number1;
            } else {
                number1 = number1 - number2;
            }
        }
        return number1;
    }

    public static void main(String[] args) {
        System.out.printf("for 2, 2: %d%n", calculateNWD(2, 2));
        System.out.printf("for 4, 2: %d%n", calculateNWD(4, 2));
        System.out.printf("for 2, 4: %d%n", calculateNWD(2, 4));
        System.out.printf("for 155, 410: %d%n", calculateNWD(155, 410));
        System.out.printf("for 3904, 410: %d%n", calculateNWD(3904, 410));
        System.out.printf("for 594, 1179: %d%n", calculateNWD(594, 1179));
    }
}