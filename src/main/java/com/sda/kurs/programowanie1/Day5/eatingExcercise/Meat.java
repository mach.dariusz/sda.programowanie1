package com.sda.kurs.programowanie1.Day5.eatingExcercise;

public class Meat implements Food {

    private String name;
    private double weight;
    private double price;
    private double calories;

    public Meat(String name, double weight, double price, double calories) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public double getCalories() {
        return calories;
    }

    @Override
    public double calcPrice(int howManyItems) {
        return howManyItems * this.price;
    }

    @Override
    public String toString() {
        return "Meat{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public double calWeight(int howManyItems) {
        return howManyItems * this.weight;
    }

    @Override
    public double calcCalories(int howManyItems) {
        return howManyItems * this.calories;
    }
}
