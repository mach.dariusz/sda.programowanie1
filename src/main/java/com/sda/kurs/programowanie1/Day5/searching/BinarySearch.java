package com.sda.kurs.programowanie1.Day5.searching;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinarySearch {

    public static void main(String[] args) {

        int[] array = {10, 20, 30, 1, 2, 3, -10, -20, 40};

        // O(n) - wyszukiwanie liniowe na niepostortowanej tablicy
        System.out.println("index: " + search(array, -10));

        // O(log n) - na posortowanej tablicy - wymagane
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.binarySearch(array, 30));
    }

    private static int search(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) return i;
        }
        return -1;
    }
}
