package com.sda.kurs.programowanie1.Day5.eatingExcercise;

import java.util.Objects;

public class Student extends Human {

    private String name;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }

    public Student(String name) {
        this.name = name;
    }

    @Override
    public void saySomething() {
        System.out.println("Student: Robie co chce! ;-)");
    }

    @Override
    public void breathe() {
        System.out.println("Student: Nawet oddychanie jest prostsze jak sie jest studentem!");
    }

    @Override
    public boolean eat(Food food) {
        if (food instanceof Fruit) {
            System.out.printf("Student: Ale dobra ta %s%n", ((Fruit) food).getName());
            return true;
        } else {
            System.out.printf("Student: Ale ja jestem biedny - co ja bym dal za %s%n", ((Meat) food).getName());
            return false;
        }
    }
}
