package com.sda.kurs.programowanie1.Day5.eatingExcercise;

import java.util.Objects;

public abstract class Human {

    protected String name;

    public void saySomething() {
        System.out.println("Employee: I znow do tej roboty ;-(");
    }


    public void breathe() {
        System.out.println("Employee: Trace oddech - zaraz padne ...");
    }


    public boolean eat(Food food) {
        if (food instanceof Fruit) {
            System.out.printf("Employee: Mniam - ale dobra ta %s i jaka zdrowa!%n", ((Fruit) food).getName());
            return true;
        } else {
            System.out.printf("Employee: Pycha - ja tam pracuje to moge, zjem %s%n", ((Meat) food).getName());
            return true;
        }
    }
}
