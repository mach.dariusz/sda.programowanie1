package com.sda.kurs.programowanie1.Day5.searching;

import com.sda.kurs.programowanie1.Day4.sets.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ComparingLists {

    public static void main(String[] args) {

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Dariusz", 32));
        employeeList.add(new Employee("Joanna", 30));
        employeeList.add(new Employee("Milosz", 3));
        employeeList.add(new Employee("Anna", 57));
        employeeList.add(new Employee("Andrzej", 59));
        employeeList.add(new Employee("Andrzej", 58));
        employeeList.add(new Employee("Andrzej", 60));
        employeeList.add(new Employee("Anna", 59));
        employeeList.add(new Employee("Jan", 59));
        employeeList.add(new Employee("Bodumila", 59));

        System.out.println("List before comparing: ");
        print(employeeList);

        employeeList.sort(Comparator.comparing(Employee::getName));
        System.out.println();
        System.out.println("List after 1st comparing: ");
        print(employeeList);

        employeeList.sort(Comparator.comparing(Employee::getName).reversed());
        System.out.println();
        System.out.println("List after 2nd comparing: ");
        print(employeeList);

        employeeList.sort(Comparator.comparing(Employee::getAge).thenComparing(Employee::getName).reversed());
        System.out.println();
        System.out.println("List after 3rd comparing: ");
        print(employeeList);

        employeeList.sort(Comparator.comparing(Employee::getAge).thenComparing(employee -> employee.getName()).reversed());
        System.out.println();
        System.out.println("List after 3rd comparing: ");
        print(employeeList);
    }

    private static void print(List<Employee> list) {
        for (Employee emp: list) {
            System.out.printf("%s%n", emp);
        }
    }
}
