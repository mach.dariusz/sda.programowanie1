### Stworz interface/klase abstrakcyjna Human
- void saySomething()
- void breathe()
- boolean eat(Food food)

### Stworz interface Food 

(klasy implementuja : name, weight, price, calories)
- double calcPrice(int howManyItems)
- double calWeight(int howManyItems)
- double calcCalories(int howManyItems)

### Stworz klasy: Employee, Student, Fruit, Meat 
- maja implementowac swoje interface'y

### Stworz metody w App.java:
- boolean feedMe(Human human, Food food); - jesli zjemy

Student - je owoce

Employee - je owoce i mieso

### Wymagania

- Program ma wyswietlac ile kto zjadl
- Dodatkowo ma miec funkcje robiaca zakupy (skorzystaj ze strutury STOS)
- Wyswietlanie kto ile zjadl (np MAP)
- w przypadku interfejsow, klas abstrakcyjnych i klas postaraj sie wykorzystac technike DRY
- wykorzystaj metody z interfejsu Human oraz Food - jesli wymagania nie sa wg Ciebie dobrze zdefiniowane, to przdefiniuj je i stworz wlasna - lepsza interpretacje kodu

Zadanie wypushujcie do gita i dajcie znac co udalo sie zrobic!