package com.sda.kurs.programowanie1.Day5.eatingExcercise;

public interface Food {

    double calcPrice(int howManyItems);
    double calWeight(int howManyItems);
    double calcCalories(int howManyItems);
}
