package com.sda.kurs.programowanie1.Day5.streams;

import com.sda.kurs.programowanie1.Day4.sets.Employee;

import java.util.*;
import java.util.stream.Collectors;

public class Collecting {

    public static void main(String[] args) {

        List<Employee> list = new ArrayList<>();
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Darek", 32));
        list.add(new Employee("Marek", 32));
        list.add(new Employee("Anna", 32));
        list.add(new Employee("Magda", 32));
        list.add(new Employee("Krzysiek", 20));
        list.add(new Employee("Mateusz", 20));
        list.add(new Employee("Grzesiek", 25));
        list.add(new Employee("Carlos", 25));
        list.add(new Employee("Piotrek", 25));
        list.add(new Employee("Sebastian", 25));
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Jan", 20));
        list.add(new Employee("Jan", 20));

        System.out.println(list);
        Set<Employee> empSet = list.stream().collect(Collectors.toSet());

        System.out.println();
        System.out.println(empSet);

        LinkedList<Employee> ll = list.stream().collect(Collectors.toCollection(LinkedList::new));
        System.out.println();
        System.out.println(ll);

        String empString = list.stream().map(Employee::getName).collect(Collectors.joining(", ")).toString();
        System.out.println();
        System.out.println(empString);

        StringJoiner joiner = new StringJoiner(", ");
        System.out.println(joiner.add("Darek").add("Marek").toString());

        Map<Boolean, List<Employee>> empMap = list.stream().collect(Collectors.partitioningBy(e -> e.getName().equals("Darek")));
        System.out.println("empMap.get(true): " + empMap.get(true));
        System.out.println("empMap.get(false): " + empMap.get(false));

        Map<Integer, List<Employee>> empMapByAge = list.stream().collect(Collectors.groupingBy(Employee::getAge));
        System.out.println("map with age 32: " + empMapByAge.get(32));
    }
}
