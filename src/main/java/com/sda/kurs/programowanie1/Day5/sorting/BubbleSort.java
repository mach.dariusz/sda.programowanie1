package com.sda.kurs.programowanie1.Day5.sorting;

import java.util.Arrays;

public class BubbleSort {

    public static Integer[] bubbleSort(Integer[] arrayToSort) {

        Integer[] array = Arrays.copyOf(arrayToSort, arrayToSort.length);
        int n = array.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            System.out.println();
            System.out.printf(" ************ Turn: %d ************ %n", i + 1);

            for (int j = 1; j < n - i; j++) {
                System.out.println();
                System.out.printf("array[%d] > array[%d] -> %d > %d %n", j-1, j, array[j - 1], array[j]);

                if (array[j -1] > array[j]) {
                    System.out.printf("-------> swap %d with %d %n", array[j - 1], array[j]);

                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;

                }

                System.out.printf("After %d -> %s %n", i + 1, Arrays.toString(array));
            }

        }

        return array;
    }

    public static void main(String[] args) {
        // int, long, float, double, boolean
        // Integer, Long, Float, Double, Boolean  <-- klasy tzw. opakowujace

        Integer[] array = {10, 22, 1, 3, 4, 5, 6};
        Integer[] array2 = array;

        System.out.println("Original array: " + Arrays.toString(array));
        Integer[] arrayAfterSort = bubbleSort(array);

        System.out.println();
        System.out.println("Array after sort: " + Arrays.toString(arrayAfterSort));
    }
}
