package com.sda.kurs.programowanie1.Day5.eatingExcercise;

import java.util.*;

public class App {

    private static final Stack<Food> MEAT_AND_FRUITS = new Stack<>();
    private static final Map<Human, List<Food>> WHAT_HAS_BEEN_EATEN = new HashMap<>();

    public static void main(String[] args) {
        zrobZakupy();
        System.out.println();

        Human student = new Student("Biedny student");
        student.breathe();
        student.saySomething();
        Human employee = new Employee("Bogaty pracownik");
        employee.breathe();
        employee.saySomething();

        while (!MEAT_AND_FRUITS.empty()) {
            boolean canStudentEat = feedMe(student, MEAT_AND_FRUITS.peek());
            if (canStudentEat) {
                MEAT_AND_FRUITS.pop();
            } else {
                feedMe(employee, MEAT_AND_FRUITS.pop());
            }
        }

        System.out.println();
        printTotal();

    }

    private static void zrobZakupy() {
        MEAT_AND_FRUITS.push(new Fruit("Czeresnia", 0.02, 0.1, 10));
        MEAT_AND_FRUITS.push(new Fruit("Sliwka", 0.1, 0.3, 50));

        MEAT_AND_FRUITS.push(new Meat("Schabowy", 10, 15, 500));
        MEAT_AND_FRUITS.push(new Meat("Szaszlyk", 20, 8.5, 1000));
        MEAT_AND_FRUITS.push(new Meat("Stek", 15, 20, 1500));

        for (Food food : MEAT_AND_FRUITS) {
            System.out.printf("Kupilem: %s%n", food);
        }
    }

    private static boolean feedMe(Human human, Food food) {
        boolean isFull = human.eat(food);

        if (isFull) {
            addFoodEatenPerHuman(human, food);
        }

        return isFull;
    }

    private static void addFoodEatenPerHuman(Human human, Food food) {
        if (WHAT_HAS_BEEN_EATEN.containsKey(human)) {
            WHAT_HAS_BEEN_EATEN.get(human).add(food);
        } else {
            List<Food> list = new ArrayList<>();
            list.add(food);
            WHAT_HAS_BEEN_EATEN.put(human, list);
        }
    }

    private static void printTotal() {
        for (Map.Entry<Human, List<Food>> entry : WHAT_HAS_BEEN_EATEN.entrySet()) {
            if (entry.getKey() instanceof Student) {
                System.out.printf("%s zjadl az %d razy%n", ((Student) entry.getKey()).getName(), entry.getValue().size());
            } else {
                System.out.printf("%s zjadl az %d razy%n", ((Employee) entry.getKey()).getName(), entry.getValue().size());
            }
        }
    }


}
