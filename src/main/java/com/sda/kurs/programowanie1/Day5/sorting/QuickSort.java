package com.sda.kurs.programowanie1.Day5.sorting;

import java.util.Arrays;

public class QuickSort {

    public static void quickSort(int array[], int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(array, begin, end);

            quickSort(array, begin, partitionIndex - 1);
            quickSort(array, partitionIndex + 1, end);
        }
    }

    private static int partition(int[] array, int begin, int end) {

        int pivot = array[end]; // <- set pivot
        int i = (begin - 1); // <- why this is set like that?

        for (int j = begin; j < end; j++) {
            if (array[j] <= pivot) { // <- if element is less or equals than pivotal
                i++; // <- initial value for i was -1, that's why we had to do begin - 1

                // swapping elements on the left from pivot
                int swapTemp = array[i];
                array[i] = array[j];
                array[j] = swapTemp;
            }
        }

        // swapping elements on the right from pivot
        int swapTemp = array[i + 1];
        array[i + 1] = array[end];
        array[end] = swapTemp;

        return i + 1;
    }

    public static void main(String[] args) {
        int[] array = {10, 20, 15, 2, 14, 3, 2, 2, 19, 58, 66, 1, 18};
        System.out.println(Arrays.toString(array));
        QuickSort.quickSort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));

    }
}

