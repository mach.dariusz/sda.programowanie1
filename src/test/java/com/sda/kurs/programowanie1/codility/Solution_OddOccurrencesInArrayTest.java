package com.sda.kurs.programowanie1.codility;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Solution_OddOccurrencesInArrayTest {

    @Test
    public void solution_passedArrayWith11Elements_shouldReturn12() throws Exception {
        int[] array = {12, 1, 3, 2, 2, 1, 3, 4, 4, 1, 1};
        Solution_OddOccurrencesInArray s = new Solution_OddOccurrencesInArray();
        Assert.assertEquals(s.solution(array), 12);
        Assert.assertEquals(s.solution_2(array), 12);
    }

    @Test
    public void solution_passed1ElementArray_shouldReturn11() throws Exception {
        int[] array = {11};
        Solution_OddOccurrencesInArray s = new Solution_OddOccurrencesInArray();
        Assert.assertEquals(s.solution(array), 11);
        Assert.assertEquals(s.solution_2(array), 11);
    }

    @Test(expected = Exception.class)
    public void solution_passed2ElementArray_shouldThrowException() throws Exception {
        int[] array = {11, 11};
        Solution_OddOccurrencesInArray s = new Solution_OddOccurrencesInArray();
        s.solution(array);
        s.solution_2(array);
    }


}