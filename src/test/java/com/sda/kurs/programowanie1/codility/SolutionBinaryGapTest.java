package com.sda.kurs.programowanie1.codility;

import com.sda.kurs.programowanie1.exceptions.OutOfRangeNumberException;
import org.junit.Assert;
import org.junit.Test;

public class SolutionBinaryGapTest {

    /*
     * N is an integer within the range [1..2,147,483,647].
     * */

    @Test(expected = OutOfRangeNumberException.class)
    public void solution_passed0_shouldReturnException () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        s.solution(0);
        s.solution_2(0);
    }

    @Test
    public void solution_passed32_shouldReturn0 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(32), 0);
        Assert.assertEquals(s.solution_2(32), 0);
    }

    @Test
    public void solution_passed1041_shouldReturn5 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(1041), 5);
        Assert.assertEquals(s.solution_2(1041), 5);
    }

    @Test
    public void solution_passed1162_shouldReturn3 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(1162), 3);
        Assert.assertEquals(s.solution_2(1162), 3);
    }

    @Test
    public void solution_passed328_shouldReturn2 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(328), 2);
        Assert.assertEquals(s.solution_2(328), 2);
    }

    @Test
    public void solution_passed561892_shouldReturn3 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(561892), 3);
        Assert.assertEquals(s.solution_2(561892), 3);
    }

    @Test
    public void solution_passed66561_shouldReturn9 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(66561), 9);
        Assert.assertEquals(s.solution_2(66561), 9);
    }

    @Test
    public void solution_passed1_shouldReturn0 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(1), 0);
        Assert.assertEquals(s.solution_2(1), 0);
    }

    @Test
    public void solution_passed2147483647_shouldReturn0 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(2_147_483_647), 0);
        Assert.assertEquals(s.solution_2(2_147_483_647), 0);
    }

    @Test
    public void solution_passed1610612737_shouldReturn28 () {
        Solution_BinaryGap s = new Solution_BinaryGap();
        Assert.assertEquals(s.solution(1_610_612_737), 28);
        Assert.assertEquals(s.solution_2(1_610_612_737), 28);
    }
}