package com.sda.kurs.programowanie1.Day1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZadaniaTest {

    @Test
    public void zadanie_3_d_passedArrayOfInts_shouldReturnArrayWithItemsDividedByThree() {

        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        int[] arrayDividedByThree = {3, 6, 9, 12, 15};

        Assert.assertArrayEquals(Zadania.zadanie_3_d(array), arrayDividedByThree);
    }

    @Test
    public void zadanie_3_d_passedArrayOfIntsWithoutItemsDividedByThree_shouldReturnEmptyArray() {

        int[] array = {1, 2, 4, 5, 7, 8, 10, 11, 13, 14};
        int[] arrayDividedByThree = {};

        Assert.assertArrayEquals(Zadania.zadanie_3_d(array), arrayDividedByThree);
    }

    @Test
    public void zadanie_3_f_passedArrayOfInts_shoudReturnSumOfFirstFour() {
        int[] array = {10, 15, 20, 25, 30, 35};

        Assert.assertEquals(Zadania.zadanie_3_f(array), 70);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void zadanie_3_f_passedArrayOfThreeInts_shoudReturnException() {
        int[] array = {10, 15, 20};
        Zadania.zadanie_3_f(array);
    }
}